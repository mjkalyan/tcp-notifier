(asdf:defsystem :tcp-notifier
  :author "mjkalyan"
  :description "A TCP server to listen for & send notifications."
  :long-description "A TCP server that sends notifications to the host machine
   using notify-send based on the UTF-8 data received."
  :license "GPL-3+"
  :version "0.3"
  :depends-on ("cl-async"
               "flexi-streams")
  :components ((:file "package")
               (:file "tcp-notifier"))

  ;; Options to automatically make a binary with (asdf:make :tcp-notifier)
  :build-operation "program-op"
  :build-pathname "tcp-notifier"
  :entry-point "tcp-notifier:main")

#+sb-core-compression
(defmethod asdf:perform ((o asdf:image-op) (c asdf:system))
  (uiop:dump-image (asdf:output-file o c) :executable t :compression t))
