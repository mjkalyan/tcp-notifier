(in-package :tcp-notifier)

(defparameter *bind-addr* "127.0.0.1")
(defparameter *port* 5000)

(defun notify-send (string)
  (uiop:run-program (list "notify-send" "TCP Notifier" string)))

(defun init-sig-handlers ()
  (as:signal-handler as:+sigint+
                     (lambda (sig)
                       (declare (ignore sig))
                       (as:exit-event-loop))))

(defun listener-daemon ()
  (tcp-server *bind-addr* *port*
    (lambda (sock data)
      (notify-send (octets-to-string data :external-format :utf-8))))
  (init-sig-handlers)
  (format t "~&Now accepting input from ~a on port ~a!" *bind-addr* *port*)
  (finish-output))

(defun set-from-read (sym)
  (let* ((val (symbol-value sym))
         (int-p (typecase val (integer t)))
         (read-val (read-line *query-io*))
         (new-var (if (= (length read-val) 0)
                      nil
                      (if int-p
                          (parse-integer read-val)
                          read-val))))
    (unless (eql new-var nil) (setf (symbol-value sym) new-var))))

(defun main ()
  (format *query-io* "Enter address to bind to (~a by default): " *bind-addr*)
  (finish-output *query-io*)
  (set-from-read '*bind-addr*)
  (format *query-io* "Enter port to listen on (~a by default): " *port*)
  (finish-output *query-io*)
  (set-from-read '*port*)
  (format t "Starting TCP server...")
  (finish-output)
  (start-event-loop #'listener-daemon))
